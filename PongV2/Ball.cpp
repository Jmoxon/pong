#include "Objects.h"
#include "Paddles.h"
#include "Ball.h"

using namespace sf;

Ball::Ball()
{
	LoadTexture();
	ball.setTexture(Tex);
	ball.setTextureRect(BC::BALL_RECT);
}

Ball::~Ball()
{
}

void Ball::LoadTexture()
{
	if (!Tex.loadFromFile("data/pongSprites.png"))
		assert(false);
	Tex.setSmooth(true); //it will look gnarly when it turns otherwise, try it. Probably using anisotropic sub pixel sampling with blur (look it up)
}

void Ball::SetInitPos(Vector2f Pos)
{
	ball.setPosition(Pos);
}

void Ball::Update(float DTime, bool started)
{
	Vector2f BallPos = ball.getPosition();
	BallPos += Velocity * DTime;
	if (started == false)
	{
		Velocity = { BC::ZERO,BC::ZERO };
	}

	if (BallPos.y <= BC::MIN_Y)
	{
		Velocity.y = -Velocity.y;
	}
	if (BallPos.y >= BC::MAX_Y)
	{
		Velocity.y = -Velocity.y;
	}

	ball.setPosition(BallPos);
}

bool Ball::CheckScored()
{
	bool Scored = false;
	if (ball.getPosition().x >= BC::MAX_X)
	{
		Scored = true;
	}
	if (ball.getPosition().x <= BC::ZERO)
	{
		Scored = true;
	}
	return Scored;
}

void Ball::Render(RenderWindow* window) const
{
	window->draw(ball);
}