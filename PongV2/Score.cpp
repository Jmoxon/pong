#include "Objects.h"
#include "Score.h"

using namespace sf;
using namespace std;

Score::Score()
{
	SetFont();
	PlayerScore = SC::ZERO;
	PScore.setFont(font);
	PScore.setCharacterSize(SC::CHAR_SIZE);
	score << PlayerScore;
	PScore.setString(score.str());
}

Score::~Score()
{
}

void Score::SetFont()
{
	if (!font.loadFromFile("data/fonts/digital-7.ttf"))
		assert(false);

}

void Score::SetInitPos(Vector2f Pos)
{
	PScore.setPosition(Pos);
}

void Score::IncreaseScore()
{

	PlayerScore += SC::INCR_SCORE;
	score.str("");
	score << PlayerScore;
	PScore.setString(score.str());
}

void Score::Render(RenderWindow* window) const
{
	window->draw(PScore);
}