#pragma once
#include <assert.h>
#include "SFML/Graphics.hpp"
#include "SFML/system.hpp"
#include "Paddles.h"
#include "Ball.h"
#include "Background.h"
#include "Score.h"

namespace OC
{
	const int ZERO = 0;
	const sf::Vector2f PAD1_INIT_POS = { 200,400 };
	const sf::Vector2f PAD2_INIT_POS = { 1100,400 };
	const sf::Vector2f BALL_INIT_POS = { 300,400 };
	const sf::Vector2f P1SCORE_INIT_POS = { 250,75 };
	const sf::Vector2f P2SCORE_INIT_POS = { 950,75 };
	const sf::Vector2f UP_VELOCITY = { 0,-300 };
	const sf::Vector2f DOWN_VELOCITY = { 0,300 };
	const int VELOCITY2 = 300;
	const int PRE_BALL_POS = 400;
	const int MAX_X = 1200;
}


class Objects
{
public:
	/* 
	* constructor for an instance of the Objects class which manages all the objects
	* Pad1 - instance of player1's paddle
	* Pad2 - instance of player2's paddle
	* ball - Instance of the ball
	* P1Score - instance of the player1's score
	* P2Score - instance of the player2's score
	* pre - Objects instance is created
	* post - all sprites original positions are set and are ready for use
	* IN - N/A
	* OUT - N/A
	*/
	Objects();
	~Objects();//deconstructor

	/* Used to load the textures of the sprites used from documents
	* Tex - Texture used to store the texture loaded
	* pre - Texture is in documents and function is called
	* post - Texture is stored and loaded properly
	* In - N/A
	* OUT - N/A
	*/
	void LoadTexture();

	/*
	* used to update all the objects in the game
	* DTime - current amount of elapsed time
	* Pad1 - instance of player1's paddle
	* Pad2 - instance of player2's paddle
	* ball - Instance of the ball
	* ObjClock - instance of a running clock used to use time in games
	* P1Score - instance of the player1's score
	* P2Score - instance of the player2's score
	* started - bool on whether the game has started or not
	* pre - Sprites have been loaded and ready for use
	* post - any inputs the player has made are recorded for rendering
	* IN - N/A
	* OUT - N/A
	*/
	void Update();

	sf::Texture Tex;// contains the texture of used for sprites

	/*
	* used to draw all the sprites and updates the screen for any changes made
	* Pad1 - instance of player1's paddle
	* Pad2 - instance of player2's paddle
	* ball - Instance of the ball
	* P1Score - instance of the player1's score
	* P2Score - instance of the player2's score
	* BG - instance of the background made
	* window - pointer to the renderwindow for the game
	* pre - sprites must be updated and loaded ready for drawing
	* post - changes are made on screen for the player to see
	* IN - window
	* OUT - n/A
	*/
	void Render(sf::RenderWindow* window) const;
private:
	sf::Clock ObjClock; // instance of the Clock class

	/*
	* used to reset the sprites back to their original positions after a player has scored
	* started - bool on whether the game has started or not
	* Pad1 - instance of player1's paddle
	* Pad2 - instance of player2's paddle
	* ball - Instance of the ball
	* pre - one of the players have scored
	* post - Sprites are all reset ready for another round of the game
	* IN - N/A
	* OUT - N/A
	*/
	void Reset(); 
	float DTime; // Current amount of time elapsed
	Paddle Pad1; // instance of one of the paddles
	Paddle Pad2;// instance of the other paddle
	Background BG; // instance of the background for the game
	Ball ball; // instance of the ball going around the screen
	Score P1Score; // instance of player1's score
	Score P2Score; // instance of player2's score
	bool Scored = false;// bool of whether a player has scored or nit
	bool started = false;// bool of whether the game has started or not
};