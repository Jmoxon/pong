#pragma once
#include <assert.h>
#include "SFML/Graphics.hpp"
#include "SFML/system.hpp"
#include "Objects.h"

namespace BC
{
	const int ZERO = 0;
	const sf::IntRect BALL_RECT = sf::IntRect(113, 14, 25, 25);
	const int MIN_Y = 50;
	const int MAX_Y = 700;
	const int MAX_X = 1200;
}
class Ball
{
public:
	/*
	* main constructor for the ball class
	* ball - Sprite used to indicate the ball the bouncs around the screen
	* pre - constructor is called
	* post - ball is loaded
	* IN - N/A
	* OUT - N/A
	*/
	Ball();
	~Ball(); // deconstuctor
	sf::Sprite ball; //Sprite used to indicate the ball the bouncs around the screen
	/*
	* Function used to update the ball and its position which lets it move around the screen
	* BallPos - Vector2 containing the balls position
	* Velocity - vector2 containing change in position of the ball
	* started - bool which says whether the game is started or not
	* IN - DTime - float of amount of time that has currently passed, started - ^
	* OUT - N/A
	* pre - sprites are initialised and function is called
	* post - ball's position has been updated to fit what the player has inputted
	*/
	void Update(float, bool);
	/* Used to load the textures of the sprites used from documents
	* Tex - Texture used to store the texture loaded
	* pre - Texture is in documents and function is called
	* post - Texture is stored and loaded properly
	* In - N/A
	* OUT - N/A
	*/
	void LoadTexture();
	sf::Texture Tex; // Texture used to store the texture of the sprite
	/*
	* Simply set the position of the ball when the game starts
	* ball - Sprite used to indicate the ball the bouncs around the screen
	* Pos - Pos given to set the original position
	* pre - function is called and ball's texture has been loaded
	* post - ball's position has been set and ready for use on screen
	* IN - Pos - Vector2 containing a position on screen for the ball to be set at
	* OUT - N/A
	*/
	void SetInitPos(sf::Vector2f);
	/*
	* Where the Sprites are drawn to be on screen
	* ball - Sprite used to indicate the ball the bouncs around the screen
	* window - pointer to the renderwindow on screen
	* pre - ball has been updated and ready to be drawn
	* post - updates are drawn on screen for the player to see
	* IN - window - pointer to the renderwindow
	* OUT - N/A
	*/
	void Render(sf::RenderWindow* window) const;
	/*
	* Checks the ball's position to see if the ball has gone off screen
	* Scored - bool used to indicate whether the ball has gone off screen
	* ball - Sprite used to indicate the ball the bouncs around the screen
	* pre - ball is moving on the screen bouncing around
	* post - Scored is changed and returned
	* IN - N/A
	* OUT - Scored
	*/
	bool CheckScored();
	sf::Vector2f Velocity; //the change in position of the balls position
private:
};