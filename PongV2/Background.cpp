#include "Objects.h"
#include "Background.h"
#include "Paddles.h"
#include "Ball.h"

using namespace sf;

Background::Background()
{
	LoadTexture();


	Backing.setSize(BGC::BACKING_SIZE);
	Backing.setFillColor(sf::Color::Blue);

	Splitter.setSize(BGC::SPLITTER_SIZE);
	Splitter.setFillColor(sf::Color::Cyan);
	Splitter.setPosition(BGC::SPLITTER_POS_X, BGC::ZERO * BGC::MAX_Y_POS);

	Bottom_Screen.setTexture(Tex);
	Bottom_Screen.setTextureRect(BGC::BS_RECT);
	Bottom_Screen.setScale(BGC::BS_SCALE_X, BGC::BS_SCALE_Y);
	Bottom_Screen.setOrigin(Tex.getSize().x / BGC::HALF, Tex.getSize().y / BGC::HALF);
	Bottom_Screen.setPosition(BGC::BS_POS_X, BGC::BS_POS_Y * BGC::MAX_Y_POS);

	Top_Screen.setTexture(Tex);
	Top_Screen.setTextureRect(BGC::TS_RECT);
	Top_Screen.setScale(BGC::TS_SCALE_X, BGC::TS_SCALE_Y);
	Top_Screen.setOrigin(Tex.getSize().x / BGC::HALF, Tex.getSize().y / BGC::HALF);
	Top_Screen.setPosition(BGC::TS_POS_X, BGC::TS_POS_Y / BGC::MIN_Y_POS);
}
Background::~Background()
{

}

void Background::LoadTexture()
{
	if (!Tex.loadFromFile("data/pongSprites.png"))
		assert(false);
	Tex.setSmooth(true); //it will look gnarly when it turns otherwise, try it. Probably using anisotropic sub pixel sampling with blur (look it up)
}
void Background::Render(RenderWindow* window) const
{
	window->draw(Backing);
	window->draw(Splitter);
	window->draw(Top_Screen);
	window->draw(Bottom_Screen);
}