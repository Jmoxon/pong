#pragma once
#include <assert.h>
#include "SFML/Graphics.hpp"
#include "SFML/system.hpp"
#include "Objects.h"


namespace PC
{
	const int ZERO = 0;
	const sf::IntRect BALL_RECT = sf::IntRect(16, 16, 16, 128);
	const int HALF = 2;
	const int COUNTERFORCE = 5;
	const int MIN_Y = 100;
	const int MAX_Y = 700;
}
class Paddle
{
public:

	Paddle();
	~Paddle();//Deconstructor
	sf::Sprite Player; //Sprite containing the paddle

	/* Used to load the textures of the sprites used from documents
	* Tex - Texture used to store the texture loaded
	* pre - Texture is in documents and function is called
	* post - Texture is stored and loaded properly
	* In - N/A
	* OUT - N/A
	*/
	void LoadTexture();
	sf::Texture Tex;// Texture used to store the texture for the sprite

	/*
	* used to update the paddle sprite and its position
	* PlayerPos - Vector2 containing the current position of the player
	* velocity - Vector2 containing the change in position
	* DTime - Float containing current amount of time elapsed
	* pre - Sprites are loaded and ready for use
	* post - any inputs made are recorded and ready for rendering
	* In - DTime
	* OUT - N/A
	*/
	void Update(float);

	/*
	* Sets the initial position of the player
	* Player - Sprite of the player's paddle
	* Pos - Pos that the Sprite will be changed to
	* pre - Player has scored or the gmae is intialising
	* post - Paddle's initial position is set
	* IN - Pos
	* OUT - N/A
	*/
	void SetInitPos(sf::Vector2f);
	sf::Vector2f Velocity; // vector2 contianing the change in position of the paddle

	/*
	* Used to draw the sprite
	* window - Pointer to a renderwindow on screen
	* Player - Sprite of the players paddle
	* pre - Updates are made and ready to be rendered
	* post - Changes are made on screen
	* IN - window
	* OUT - N/A
	*/
	void Render(sf::RenderWindow*) const ;
private:

};