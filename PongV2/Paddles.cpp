#include "Objects.h"
#include "Paddles.h"

using namespace sf;

Paddle::Paddle()
{
	LoadTexture();
	Player.setTexture(Tex);
	Player.setTextureRect(PC::BALL_RECT);
	Player.setOrigin(Player.getTextureRect().width / PC::HALF, Player.getTextureRect().height / PC::HALF);
}
Paddle::~Paddle()
{

}

void Paddle::LoadTexture()
{
	if (!Tex.loadFromFile("data/pongSprites.png"))
		assert(false);
	Tex.setSmooth(true); //it will look gnarly when it turns otherwise, try it. Probably using anisotropic sub pixel sampling with blur (look it up)
}
void Paddle::Update(float DTime)
{
	Vector2f PlayerPos = Player.getPosition();
	if (PlayerPos.y > PC::MAX_Y)
	{
		Velocity = { PC::ZERO,-PC::COUNTERFORCE };
	}
	if (PlayerPos.y < PC::MIN_Y)
	{
		Velocity = { PC::ZERO,PC::COUNTERFORCE };
	}
	PlayerPos += Velocity * DTime;
	Velocity = { PC::ZERO, PC::ZERO };
	Player.setPosition(PlayerPos);
}

void Paddle::SetInitPos(Vector2f Pos)
{
	Player.setPosition(Pos);
}

void Paddle::Render(RenderWindow* window) const
{
	window->draw(Player);
}
