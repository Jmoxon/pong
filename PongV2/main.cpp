#include <assert.h>
#include <ctime>		//for time used in random number routines
#include <iostream>		//for cin >> and cout <<
#include <string>		//for string routines
#include "SFML/Graphics.hpp"
#include "Objects.h"

using namespace std;
using namespace sf;
//dimensions in 2D that are whole numbers
struct Vec2i
{
	int x, y;
};

//dimensions in 2D that are floating point numbers
struct Vec2f
{
	float x, y;
};

//a namespace is just another type of box with a label where we can group things together
//GDC="GameDataConstants" - a box for all your magic numbers
//especially those that a designer might need to tweak to balance the game
namespace GDC
{
	const Vec2i SCREEN_RES{ 1200,800 };	//desired resolution
	const char ESCAPE_KEY{ 27 };		//ASCII code
	const char ENTER_KEY{ 13 };
	const sf::Uint32 ASCII_RANGE{ 127 };//google an ASCII table, after 127 we don't care
	const float PLAY_SPIN_SPD{ 100.f };	//how fast to spin
	const int ZERO = 0;
	const int FONT_SIZE = 30;
	const int TXT_POS = 100;
}

//keep important shared information "GameData" in one accessible object and pass it around
//note - structs are like classes but everything is always public, so best kept just for data
struct GD
{
	sf::Font font;				//a shared font to use everywhere
	sf::RenderWindow* pWindow;	//can't render anything without this
	string playerName;			//the player's name is needed by different objects
};

class Game;

//in this mode we want the player's name, like a game's frontend menu screens
class NameMode
{
public:
	NameMode() :mpGame(nullptr) {}
	//setup
	void Init(Game*);
	//handle any logic
	void Update() {};
	//display
	void Render();
	//process windows text messages
	void TextEntered(char);
private:
	Game* mpGame;	//the only way to communicate with the rest of the game
};

//in this mode the player can spin the screen, like a game's playable part
//similar structure to the NameMode, imagine more of these objects for each
//significant part of the game, all structured the same way
class PlayMode
{
public:
	PlayMode() : mpGame(nullptr), mAngle(GDC::ZERO) {}
	void Init(Game*);
	void Update();
	void Render();
private:
	Game* mpGame; // a pointer type of the game instance used to communicate with the game
	Objects Objects; //instance of the objects needed for the game
	float mAngle;			//current angle of spin
};


//manage the game's different behaviours
class Game
{
public:
	//control what the game is doing 
	enum class StateMachine {
		WAITING_INIT,	//not in a safe state yet
		GET_NAME,		//what's your name?
		PLAY			//let's play!!
	};

	Game();
	//setup
	void Initialise(sf::RenderWindow&);
	//logic
	void Update();
	//display
	void Render();
	//input from windows
	void TextEntered(char);

	//accessors
	StateMachine& GetState() { return mState; }
	void SetState(StateMachine nState) { mState = nState; }

	GD& GetData() { return mGD; }
private:
	GD mGD;				//shared game data, be careful here we are breaking rules of encapsulation
	StateMachine mState;//contols what's going on in the 
	PlayMode mPMode;	//object used for playing the game (like a mini game)
	NameMode mNMode;	//object used for getting the player's name (like a mini frontend)
};

/******************************************************************
game implementation code
Note that we can't give mPMode and mNMode constructors, for example: PlayMode(Game&)
which seems obvious and elegant, because the Game object itself wouldn't have finished
constructing, so if in one of those mode objects, it started using the Game& reference,
that would be bad as the Game object might not be completely ready (hence Initialise(Game*)
functions instead). We will learn how to get around that using dynamic allocation later.
*/
Game::Game()
	: mState(StateMachine::WAITING_INIT)
{}

/*
* this is where all the modes are initilaised to load 
* mState - The current state in which the game is in
* mNMode - instance of the name mode state
* mPMode - instance of the play mode state
* IN - instance of the game window
* OUT - N/A
* pre - window has been created and game is waiting to be initialised
* post - all modes are initialised and ready for use and mode is switched the the name mode
*/
void Game::Initialise(sf::RenderWindow& window)
{
	assert(mState == StateMachine::WAITING_INIT);
	mGD.pWindow = &window;
	//load the font
	if (!mGD.font.loadFromFile("data/fonts/comic.ttf"))
		assert(false);

	//setup
	mNMode.Init(this);
	mPMode.Init(this);
	mState = StateMachine::GET_NAME;
}

/*
* This is where the game gets updated based on which state the game is currently in
* IN - n/a
* OUT - N/A
* pre - everything shoudl be initialised and the state should either be in playmode or namemode
* post - the game state is updated
*/
void Game::Update()
{
	switch (mState)
	{
	case StateMachine::GET_NAME:
		mNMode.Update();
		break;
	case StateMachine::PLAY:
		mPMode.Update();
		break;
	}
}

/*used to render each objct to make sure they appear on screen
* IN - N/A
* OUT - N/A
* pre - Game should've been initialised and updated ready for this function to render the objects
* post - Any updates should be rendered on screen
*/
void Game::Render()
{
	switch (mState)
	{
	case StateMachine::GET_NAME:
		mNMode.Render();
		break;
	case StateMachine::PLAY:
		mPMode.Render();
		break;
	}

}

void Game::TextEntered(char key)
{
	//escape overrides everything else, just quit
	switch (key)
	{
	case GDC::ESCAPE_KEY:
		mGD.pWindow->close();
		break;
	}

	//pass input messages to different mode objects
	switch (mState)
	{
	case StateMachine::GET_NAME:
		mNMode.TextEntered(key);
		break;
	case StateMachine::PLAY:
		break;
	default:
		assert(false);
	}
}





//******************************************************************
//boilerplate setup and manage your game
class Application
{
public:
	Application();
	//this kicks off everything
	void Run();
	//global way to see how much time passed since the last update
	static float GetElapsedSecs() { return sElapsedSecs.asSeconds(); }
private:
	Game mGame;
	sf::RenderWindow mWindow;		//for rendering
	static sf::Time sElapsedSecs;	//track how much time each update/render takes for smooth motion
};
sf::Time Application::sElapsedSecs;	//as it's static it need instantiating separately

Application::Application()
{
	// Create the main window
	mWindow.create(sf::VideoMode(GDC::SCREEN_RES.x, GDC::SCREEN_RES.y), "Pong");
	mGame.Initialise(mWindow);
}

void Application::Run()
{
	// Start the game loop 
	sf::Clock clock;
	while (mWindow.isOpen())
	{
		// Process events
		sf::Event event;
		while (mWindow.pollEvent(event))
		{
			// Close window: exit
			if (event.type == sf::Event::Closed)
				mWindow.close();
			if (event.type == sf::Event::TextEntered)
			{
				if (event.text.unicode < GDC::ASCII_RANGE)
					mGame.TextEntered(static_cast<char>(event.text.unicode));
			}
		}

		sElapsedSecs = clock.restart();

		// Clear screen
		mWindow.clear();

		mGame.Update();
		mGame.Render();

		// Update the window
		mWindow.display();
	}
}

//******************************************************************

/*
* Simply initialises the namemode with any variables it needs
* mpGame - pointer to game instance
* IN - pG - Pointer to game
* OUT - N/A
* pre - game is loaded and window has been created
* post - namemode is fully ready for use
*/
void NameMode::Init(Game* pG)
{
	assert(!mpGame);
	mpGame = pG;
}

/*
* Used to enter characters while the game is playing ready to be put on screen
* nm - string pointer used to store the name
* key - char that the player has entered
* mpGame - pointer to game instance
* pre - key is pressed
* post - char is stored in string that contains the name
* IN - char that is the player's input
* OUT - n/A
*/
void NameMode::TextEntered(char key)
{
	assert(mpGame);
	string& nm = mpGame->GetData().playerName;
	if (key == GDC::ENTER_KEY && nm.length() > GDC::ZERO)
	{
		mpGame->SetState(Game::StateMachine::PLAY);
	}
	else
	{
		if (isalpha(key))
			nm += key;
	}
}

/* Renders text on screen for the player to see
* txt - Text that stores the message of the players name
* mssg - string that stores the message that displays on screen
* pre - Name has been inputted
* post - Is shown on screen to see
* IN - N/A
* OUT - N/A
*/
void NameMode::Render()
{
	assert(mpGame);
	string mssg = "Type your name (press enter when complete): ";
	mssg += mpGame->GetData().playerName;
	sf::Text txt(mssg, mpGame->GetData().font, GDC::FONT_SIZE);
	txt.setPosition(GDC::TXT_POS, GDC::TXT_POS);
	mpGame->GetData().pWindow->draw(txt);
} 

//******************************************************************
//a mini playable game (sort of)

/*
* Simply initialises the Playmode with any variables it needs
* mpGame - pointer to game instance
* IN - pG - Pointer to game
* OUT - N/A
* pre - State has changed to PlayMode
* post - PlayMode is fully ready for use
*/
void PlayMode::Init(Game* pG)
{
	mpGame = pG;
}

/*
* Updates the game based on functions used
* mpGame - pointer to game instance
* gd - Where game data is stored
* IN - N/A
* OUT - N/A
* pre - Playmode has been initialised
* post - Objects on screen have been updated with any inputs from the player
*/
void PlayMode::Update()
{
	assert(mpGame);
	GD& gd = mpGame->GetData();
	
	Objects.Update();
}

/*
* Renders any changes made and draws the sprites
* mpGame - pointer to game instance
* gd - Where game data is stored
* IN - N/A
* OUT - N/A
*/
void PlayMode::Render()
{
	//the instructions
	assert(mpGame);
	GD& gd = mpGame->GetData();
	Objects.Render(gd.pWindow);
}


//********************************************
//where it all starts
int main()
{
	Application app;
	app.Run();

	return EXIT_SUCCESS;
}
