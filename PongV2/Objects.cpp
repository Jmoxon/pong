#include "Objects.h"
#include "Paddles.h"
#include "Ball.h"

using namespace sf;

Objects::Objects()
{
	Pad1.SetInitPos(OC::PAD1_INIT_POS);
	Pad2.SetInitPos(OC::PAD2_INIT_POS);
	ball.SetInitPos(OC::BALL_INIT_POS);
	P1Score.SetInitPos(OC::P1SCORE_INIT_POS);
	P2Score.SetInitPos(OC::P2SCORE_INIT_POS);
}
Objects::~Objects()
{
}

void Objects::Update()
{
	DTime = ObjClock.getElapsedTime().asSeconds();
	if (Keyboard::isKeyPressed(Keyboard::W))
	{
		Pad1.Velocity = OC::UP_VELOCITY;
		if (started == false)
		{
			ball.Velocity = OC::UP_VELOCITY;
		}
	}
	if (Keyboard::isKeyPressed(Keyboard::S))
	{
		Pad1.Velocity = OC::DOWN_VELOCITY;
		if (started == false)
		{
			ball.Velocity = OC::DOWN_VELOCITY;
		}
	}
	if (Keyboard::isKeyPressed(Keyboard::Up))
	{
		Pad2.Velocity.y = -OC::VELOCITY2;
	}
	if (Keyboard::isKeyPressed(Keyboard::Down))
	{
		Pad2.Velocity.y = OC::VELOCITY2;
	}
	if (Keyboard::isKeyPressed(Keyboard::Space))
	{
		if (started == false)
		{
			ball.Velocity.x = OC::VELOCITY2;
			ball.Velocity.y = Pad1.Player.getPosition().y - OC::PRE_BALL_POS;
			started = true;
		}
	}

	if (ball.ball.getPosition().x >= OC::MAX_X)
	{
		P1Score.IncreaseScore();
		Reset();
	}
	if (ball.ball.getPosition().x <= OC::ZERO)
	{
		P2Score.IncreaseScore();
		Reset();
	}

	if (ball.ball.getGlobalBounds().intersects(Pad1.Player.getGlobalBounds()) == true)
	{
		ball.Velocity.x = -ball.Velocity.x;

	}
	if (ball.ball.getGlobalBounds().intersects(Pad2.Player.getGlobalBounds()) == true)
	{
		ball.Velocity.x = -ball.Velocity.x;
	}

	ball.Update(DTime, started);
	Pad1.Update(DTime);
	Pad2.Update(DTime);
	ObjClock.restart();
}

void Objects::Render(RenderWindow* window) const
{
	BG.Render(window);
	P1Score.Render(window);
	P2Score.Render(window);
	Pad1.Render(window);
	Pad2.Render(window);
	ball.Render(window);
}

void Objects::Reset()
{
	started = false;
	Pad1.SetInitPos(OC::PAD1_INIT_POS);
	Pad2.SetInitPos(OC::PAD2_INIT_POS);
	ball.SetInitPos(OC::BALL_INIT_POS);
}