#pragma once
#include <assert.h>
#include "SFML/Graphics.hpp"
#include "SFML/system.hpp"
#include "Objects.h"
#include <string>
#include <sstream>

namespace SC
{
	const int ZERO = 0;
	const int CHAR_SIZE = 100;
	const int INCR_SCORE = 1;
}

class Score
{
public:
	/*
	* Constructor that initialises the score class
	* PlayerScore - integer where the score is stored
	* PScore - Text where the string is shown
	* score - Stringstream contaning a astring which is then displayed
	* font - Font that is used
	* pre - Score class is created
	* post - Score instance is initialised
	* IN - N/A
	* OUT - N/A
	*/
	Score();
	~Score();// Deconstructor

	/*
	* Used to increase the Player's score whenever the ball goes off screen
	* PlayerScore  - integer where the score is stored
	* score - Stringstream contaning a astring which is then displayed
	* PScore - Text where the string is shown
	* pre - Player must've scored
	* post - Score and screen displayed will be updated
	* IN - N/A
	* OUT - N/A
	*/
	void IncreaseScore();

	/*
	* Sets the initial position of the score text
	* PScore - Text containing the score
	* Pos - Pos that the Text will be changed to
	* pre - Objects are initialising
	* post - Text's initial position is set
	* IN - Pos
	* OUT - N/A
	*/
	void SetInitPos(sf::Vector2f);

	/*
	* Used to draw the text
	* window - pointer to the renderwindow
	* PScore - Text where the string is shown
	* pre - Updates are made and are ready to be drawn
	* post - Text is updated on screen with up to date score
	* IN - window
	* OUT - N/A
	*/
	void Render(sf::RenderWindow*) const;
private:
	sf::Text PScore; // Text where the string is shown
	int PlayerScore; // integer where the score is stored

	/*
	* Loads and sets the font of the text
	* font - font that is used
	* pre - Score instance is created
	* post - font is properly loaded using file from documents
	* IN - N/A
	* OUT - N/A
	*/
	void SetFont();
	std::stringstream score; // Stringstream contaning a astring which is then displayed
	sf::Font font; //Font that is loaded and used
};
