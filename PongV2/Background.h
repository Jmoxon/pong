#pragma once
#include <assert.h>
#include "SFML/Graphics.hpp"
#include "SFML/system.hpp"

namespace BGC
{
	const int ZERO = 0;
	const sf::Vector2f BACKING_SIZE = { 1200,800 };
	const sf::Vector2f SPLITTER_SIZE = { 10, 800 };
	const int BS_SCALE_X = 12;
	const int BS_SCALE_Y = 2;
	const int TS_SCALE_X = 12;
	const int TS_SCALE_Y = 2;
	const sf::IntRect BS_RECT = sf::IntRect(100, 100, 250, 150);
	const sf::IntRect TS_RECT = sf::IntRect(100, 50, 250, 50);
	const int HALF = 2;
	const float MAX_Y_POS = 1.05f;
	const float MIN_Y_POS = 5.5f;
	const int TS_POS_X = 1200;
	const int TS_POS_Y = 800;
	const int BS_POS_X = 1200;
	const int BS_POS_Y = 800;
	const int SPLITTER_POS_X = 600;
}
class Background
{
public:
	/*Main constructor for the background
	* Top_Screen - Sprite for the bar at the top of the screen
	* Bottom_Screen - Sprite for the bar at the bottom of the screen
	* Backing - coloured rectangleshape used to add colour to the background
	* Splitter - Coloured rectangle used to split the screen into two parts
	* In - N/A
	* OUT - N/A
	* pre - background is called
	* post - background sprites are initialised and loaded
	*/
	Background();
	~Background();
	sf::Texture Tex; //Used to store any textures used
	/* Used to load the textures of the sprites used from documents
	* Tex - Texture used to store the texture loaded
	* pre - Texture is in documents and function is called
	* post - Texture is stored and loaded properly
	* In - N/A
	* OUT - N/A
	*/
	void LoadTexture();
	/*
	* used to draw all sprites inialised and loaded
	* Top_Screen - Sprite for the bar at the top of the screen
	* Bottom_Screen - Sprite for the bar at the bottom of the screen
	* Backing - coloured rectangleshape used to add colour to the background
	* Splitter - Coloured rectangle used to split the screen into two parts
	* IN - window - constant pointer to the renderwindow
	* OUT - N/A
	* pre - Sprites are fully loaded and textured
	* post - Sprites are drawn on screen
	*/
	void Render(sf::RenderWindow* window) const;
private:
	sf::Sprite Top_Screen; //Sprite for the bar at the top of the screen
	sf::Sprite Bottom_Screen; //Sprite for the bar at the bottom of the screen
	sf::RectangleShape Backing;//coloured rectangleshape used to add colour to the background
	sf::RectangleShape Splitter; //Coloured rectangle used to split the screen into two parts
};